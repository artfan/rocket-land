$('document').ready(function(){
    let stepInput = $('.one-quest input'),
    nextStepBtn = $('.next-step'),
    stepsList, body;
    body = $('body');
    stepsList = $('.one-quest');
    if($.cookie('visited')){
        console.log(true);
        body.addClass('landing').removeClass('login');
    } else {
        console.log(false);
        body.addClass('login').removeClass('landing');

    }
    $('.menu-mobile').click(function(){
        $('.header').toggleClass('header--menu-open');
    })

    $('.quest-number__all').html(stepsList.length);
    stepInput.on('change', function(){
        if (stepInput.val){
            nextStepBtn.prop('disabled', false);
        }
    })
    nextStepBtn.on('click', function(){
        stepSwitch();
    })

    function stepSwitch(){
        let nextQuest = $('.one-quest--active').next(),
        activeQuest;
        activeQuest = $('.one-quest--active');
        activeQuest.addClass('one-quest--passed').removeClass('one-quest--active');
        nextQuest.addClass('one-quest--active').removeClass('one-quest--hidden');
        $('.quest-number__active').html(nextQuest.data('number'));
        nextStepBtn.prop('disabled', true);
        let progress = 100 * nextQuest.data('number') / stepsList.length;
        $('.quest__ready').css('width', progress+'%' );
        if (activeQuest.hasClass('one-quest--final')){
            body.removeClass('quest-page').addClass('loading');
            
            $('.wrapper--quest').hide(300);
            // $('.wrapper--loading').show(300);
            setTimeout(openPrice, 6300);
        }
    }
    function openPrice(){
        body.removeClass('loading').addClass('price');
    }

    $('.start').on('click', function(){
        // if (!$.cookie('visited')){
            body.removeClass('login landing').addClass('quest-page');
        // }
    })
    $('a.scrollto').on('click', function() {let href = $(this).attr('href'); $('html, body').animate({scrollTop: $(href).offset().top }, {duration: 370, easing: "linear"}); return false; });

    $('.price-all__form').on('submit', function(e){
        e.preventDefault();
        $.cookie('price', $('input[name="price"]:checked').val());
        $.cookie('visited', true);
        console.log($.cookie('price'));
        body.removeClass('price').addClass('landing');
        $('input[value='+ $.cookie('price') +']').attr('checked', true);
    })
    $('input[value='+ $.cookie('price') +']').attr('checked', true);
    $('.order__form').on('submit', function(e){
        e.preventDefault();
    })
    $('.feedbacks__list').slick({
        arrows: false,
        autoplay: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        autoplaySpeed: 3000,
          centerMode: true,
        responsive: [
            {
              breakpoint: 768,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
              }
            },
            {
              breakpoint: 1350,
              settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2,
              }
            },
        ],
        
    });
    preLoad();
})
function preLoad(){
    $('.preloader').hide();
}